# INTRODUCTION 

Dockerized app, npm packages, executables, infra as code and more ... 

Here you will find my most interesting projects. In Furthermore, some have a :bullettrain_side: quick demo available,  but  :warning:**WARNING**:warning:  , not all of my projects are available in quick demo
- Quick trials required **linux** distribution to execute `makefile`
- Some executable (.exe) required **windows** distribution to be launched


:mag: You can find the exhaustive list of my projects in the **groups**  section of my [profile](https://gitlab.com/enzo-cora)

### GROUPS :
- [`projects`](https://gitlab.com/projects-and-chill) 
- [`infra as code`](https://gitlab.com/infrastructure-as-code6) 
- [`models`](https://gitlab.com/models8)
- [`sandbox`](https://gitlab.com/sandbox180)


# PORTFOLIO  :


### Who am i game - `web app`


| Repo | Technos & Technics
|:--:|:--:|
| [front game interface](https://gitlab.com/projects-and-chill/who-am-i/front-who-am-i) | `react` `socketio`
| [backend websocket](https://gitlab.com/projects-and-chill/who-am-i/api-who-am-i)| `socketio` `typescript` `mongodb`  
| [terraform_iac](https://gitlab.com/projects-and-chill/who-am-i/terraform-who-am-i) |  `terraform` `ci/cd` `gitlab-ci` `gitlab-flow`

:fast_forward: **fast trial** : [docker-who-am-i](https://gitlab.com/projects-and-chill/who-am-i/docker-env-who-am-i) 


---
---
---
---


### Crypto arbitrage v1   - `web app`


| Repo | Technos & Technics
|:--:|:--:|
| [front admin pannel](https://gitlab.com/projects-and-chill/arbitrage-v1/admin-pannel-service) | `angular`
| [api crypto filtrer service](https://gitlab.com/projects-and-chill/arbitrage-v1/filter-symbols-service)| `nodejs` `mongodb` `typescript`

:fast_forward: **fast trial** : [docker-cryptpo-arbitrage-v1](https://gitlab.com/projects-and-chill/arbitrage-v1/docker-crypto-arbitrage) 


---
---
---
---


### Gitlab-ci pipeline  - `ci/cd`  - `model`


| Repo | Technos & Technics
|:--:|:--:|
| [backend_pipeline](https://gitlab.com/models8/pipeline-model-group/api-pipeline) | `ci/cd` `gitlab-ci` `gitlab-flow` `unit-tests` `integration-tests` `e2e-tests`
| [frontend_pipeline](https://gitlab.com/models8/pipeline-model-group/frontend-pipeline) | `ci/cd` `gitlab-ci` `gitlab-flow` `e2e-tests`
| [terraform_iac](https://gitlab.com/models8/pipeline-model-group/terraform-playbook-pipeline) |  `terraform` `ci/cd` `gitlab-ci` `gitlab-flow` `e2e-tests`
| [ci_templates](https://gitlab.com/models8/pipeline-model-group/gitlab-ci-templates) | `scripts` `gitlab-ci` `bash` 
| [docker_env](https://gitlab.com/models8/pipeline-model-group/docker-environments-pipeline-model) | `docker-compose`

:fast_forward:  **fast trial** : [gitlab group pipeline](https://gitlab.com/models8/pipeline-model-group)


---
---
---
---


### Crypto arbitrage v2  - `big data` - `microservice`

| Repo | Technos & Technics
|:--:|:--:|
| [big data calculator](https://gitlab.com/projects-and-chill/arbitrage-v2/arbitrage-calculator) | `typescript` `nodejs` `rabitmq` `clean architecture` `mongodb`

:fast_forward: **fast trial** :  NO

---
---
---
---



### Component creator cli   - `npm package` `cli`

| Repo | Technos & Technics
|:--:|:--:|
| [component creator](https://gitlab.com/projects-and-chill/component-creator-cli) | `typescript` `cli` `nodejs`

:fast_forward: **fast trial** :  [npm-package](https://www.npmjs.com/package/component-creator-cli)

---
---
---
---


### Terraform web app deployment  AWS - `infra as code` 


| Repo | Technos & Technics
|:--:|:--:|
 [tf-arbitrage-aws-cost-optimized](https://gitlab.com/infrastructure-as-code6/terraform/terraform-aws-low-cost-arbitrage) | `terraform` `aws` `ecs` `cluster` `load balancing` `scaling` `hight availability`
[tf-pipeline-model](https://gitlab.com/models8/pipeline-model-group/terraform-playbook-pipeline) | `terraform` `load balancing` `aws` `ecs` `cluster` `multi-environment` `scaling` `hight availability` `rolling update`

:fast_forward:  **fast trial** : see above - *Each Iac project above is independent*


---
---
---
---


### DDD & Event Sourcing  - `Architecture` - `model` 

| Repo | Technos & Technics
|:--:|:--:|
| [DDD_model](https://gitlab.com/models8/market-place-ddd/DDD-market-place) | `DDD` `microservice` `SOLID` `clean-architecture`  `event-driven` `event-sourcing` `eventstore-db` `mongodb` `redis` `rabbitmq` `nodejs`

:fast_forward: **fast trial** :  NO


---
---
---
---



### Clean Architecture  - `Architecture` - `model` 

| Repo | Technos & Technics
|:--:|:--:|
[clean architecture](https://gitlab.com/models8/clean-architecture-model) | `clean architecture` `structure`

:fast_forward:  **fast trial** : No


---
---
---
---

### CloudFormation (aws) deployment   - `Infra as code` 


| Repo | Technos & Technics
|:--:|:--:|
 [cf-arbitrage-aws-cost-optimized](https://gitlab.com/infrastructure-as-code6/aws-cloud-formation/cloudformatin-low-cost-arbitrage) | `cloudFormation` `ecs` `aws` `ecs` `cluster` `load balancing` `scaling` `hight availability`
 [cf-arbitrage-aws-infra-optimized](https://gitlab.com/infrastructure-as-code6/aws-cloud-formation/cloudformatin-arbitrage) | `cloudFormation` `aws` `ecs` `cluster` `load balancing` `auto scaling` `hight availability` `metrics`
  [cf-mongo-instance](https://gitlab.com/infrastructure-as-code6/aws-cloud-formation/cloudformatin-mongo-singleton) | `cloudFormation` `aws` `ec2`


:fast_forward:  **fast trial** : see above - *Each Iac project above is independent*

---
---
---
---


### Ansible Playbook Gitlab Runners    - `Infra as code` 


| Repo | Technos & Technics
|:--:|:--:|
[playbook gitlab-runner](https://gitlab.com/infrastructure-as-code6/ansible/playbooks/ansible-playbook-linux-gitlab-runner) | `ansible` `playbook`
[role gitlab-runner install](https://gitlab.com/infrastructure-as-code6/ansible/roles/ansible-role-gitlab-runner-install) | `ansible`  `apt`  `shell`  `copy`
[role gitlab-runner configure](https://gitlab.com/infrastructure-as-code6/ansible/roles/ansible-role-linux-gitlab-runner) | `ansible` `template`  `shell`  `loop`


:fast_forward:  **fast trial** : [ansible-playbook-gitlab-runner](https://gitlab.com/infrastructure-as-code6/ansible/playbooks/ansible-playbook-linux-gitlab-runner)

---
---
---
---

### Sudoku Resolver  - `programme`


| Repo | Technos & Technics
|:--:|:--:|
| [sudoku resolver](https://gitlab.com/projects-and-chill/sudoku-resolver) | `typescript` `nodejs` `algorithmic`

**:fast_forward: fast trial** : [sudoku-resolver](https://gitlab.com/projects-and-chill/sudoku-resolver) 


---
---
---
---


###  Immobilier  POF - `web app`

| Repo |Technos & Technics
|:--:|:--:|
| [front immobilier](https://gitlab.com/projects-and-chill/immobilier/angular-front-immobilier) | `angular`
| [api immobilier](https://gitlab.com/projects-and-chill/immobilier/nodejs-api-immobilier)| `nodejs` `mongodb`

:fast_forward: **fast trial** : [docker-immo-angular](https://gitlab.com/projects-and-chill/immobilier/docker-immo-angular) 

---
---
---
---

###  Immobilier POF  (no stylized) - `web app`


| Repo| Technos & Technics
|:--:|:--:|
| [front immobilier](https://gitlab.com/projects-and-chill/immobilier/react-front-immobilier)| `react` `redux` `rxjs`
| [api immobilier](https://gitlab.com/projects-and-chill/immobilier/nodejs-api-immobilier) | `nodejs` `mongodb`

:fast_forward: **fast trial** : [docker-immo-react](https://gitlab.com/projects-and-chill/immobilier/docker-immo-react) 


---
---
---
---

### Mario Sokoban Game - `game` 

| Repo | Technos & Technics
|:--:|:--:|
| [mario sokoban](https://gitlab.com/projects-and-chill/mario-game-sokoban) | `c++`

**:fast_forward: fast trial** : [mario-sokoban-v7](https://gitlab.com/projects-and-chill/mario-game-sokoban/-/releases/v7) 

---
---
---
---

# :arrow_right: [THX FOR WATCHING, SEE ALL OF MY PROJECTS HERE HERE](https://gitlab.com/users/enzo-cora/groups) :arrow_left: 

